CREATE TABLE "Group"
(
    "ID" INTEGER DEFAULT nextval('"Group_ID_seq"'::regclass) PRIMARY KEY NOT NULL,
    "Name" VARCHAR(50),
    "Number" BIGINT
);
CREATE UNIQUE INDEX "Group_ID_uindex" ON "Group" ("ID");
CREATE TABLE journal
(
    id INTEGER DEFAULT nextval('journal_id_seq'::regclass) PRIMARY KEY NOT NULL,
    studentfk BIGINT,
    lectionfk BIGINT,
    date DATE,
    CONSTRAINT journal_students_id_fk FOREIGN KEY (studentfk) REFERENCES students (id),
    CONSTRAINT journal_lection_id_fk FOREIGN KEY (lectionfk) REFERENCES lection (id)
);
CREATE UNIQUE INDEX "Journal_ID_uindex" ON journal (id);
CREATE TABLE lection
(
    id INTEGER DEFAULT nextval('lection_id_seq'::regclass) PRIMARY KEY NOT NULL,
    name VARCHAR(50),
    text VARCHAR(2000)
);
CREATE UNIQUE INDEX "Lection_ID_uindex" ON lection (id);
CREATE TABLE students
(
    id INTEGER DEFAULT nextval('student_id_seq'::regclass) PRIMARY KEY NOT NULL,
    name VARCHAR(50),
    birthday DATE,
    sex INTEGER,
    groupfk BIGINT,
    CONSTRAINT student_group_id_fk FOREIGN KEY (groupfk) REFERENCES "Group" ("ID")
);
CREATE UNIQUE INDEX "Student_ID_uindex" ON students (id);