package dp9v.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dpolkovnikov on 17.02.17.
 */
public class Lection extends IDRecord {
	String name;
	String text;

	private Lection(ResultSet set) throws SQLException {
		super(set.getLong("ID"));
		this.name = set.getString("Name");
		this.text = set.getString("Text");
	}

	public Lection(String name, String text) throws SQLException, RecordNotCreateException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "INSERT INTO main.lection (name, text) " +
		               "VALUES ('" + name + "', '" + text + "')" +
		               "RETURNING \"id\";";
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			this.name = name;
			this.text = text;
			this.setId(res.getLong("ID"));
		} else
			throw new RecordNotCreateException("не удалось создать запись");
	}

	public static Lection get(long id) throws SQLException, RecordNotFindException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"lection\"\n" +
		               "WHERE \"lection\". \"id\" = " + id;
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			Lection lec = new Lection(res);
			return lec;
		} else
			throw new RecordNotFindException("Не найдена запись");
	}

	public static ArrayList<Lection> filterByName(String name) throws SQLException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"lection\"\n" +
		               "WHERE \"lection\".\"name\" ='" + name + "'";
		ResultSet          res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Lection> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Lection(res));
		}
		return resultArray;
	}

	public static ArrayList<Lection> all() throws SQLException {
		SQLExcecutor       excecutor   = SQLExcecutor.getInstance();
		String             query       = "SELECT * FROM main.\"lection\"";
		ResultSet          res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Lection> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Lection(res));
		}
		return resultArray;
	}

	public void save() throws SQLException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "UPDATE main.lection SET name = '"+this.getName()+"', text = '"+this.getText()+"' " +
		               "WHERE id = "+ this.getId() +" RETURNING \"id\";";
		excecutor.excecuteQuery("Innopolis", query, new String[] {});
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
