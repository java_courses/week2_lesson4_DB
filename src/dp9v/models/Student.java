package dp9v.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by dpolkovnikov on 17.02.17.
 */
public class Student extends IDRecord {
	String name;
	int    sex;
	Date   birthday;
	Group  group;

	private Student(ResultSet set) throws SQLException, RecordNotFindException, ClassNotFoundException {
		super(set.getLong("ID"));
		Group gr = Group.get(set.getLong("GroupFK"));
		this.name = set.getString("name");
		this.sex = set.getByte("Sex");
		this.birthday = set.getDate("birthday");
		this.group = gr;
	}

	public Student(String name, int sex, Date birthday, Group group) throws SQLException, RecordNotCreateException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "INSERT INTO main.students (name, birthday, sex, groupfk) " +
		               "VALUES ('" + name + "', '" + birthday.toString() + "', " + sex + ", " + group.getId() + ")" +
		               "RETURNING \"id\";";
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			this.name = name;
			this.sex = sex;
			this.birthday = birthday;
			this.group = group;
			this.setId(res.getLong("ID"));
		} else
			throw new RecordNotCreateException("не удалось создать запись");
	}

	public static Student get(long id) throws ClassNotFoundException, SQLException, RecordNotFindException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"students\"\n" +
		               "WHERE \"students\". \"id\" = " + id;
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			Student st = new Student(res);
			return st;
		} else
			throw new RecordNotFindException("Не найдена запись");
	}

	public static ArrayList<Student> filterByName(
			String name) throws SQLException, RecordNotFindException, ClassNotFoundException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"students\"\n" +
		               "WHERE \"students\".\"name\" like'" + name + "'";
		ResultSet          res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Student> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Student(res));
		}
		return resultArray;
	}

	public static ArrayList<Student> all() throws SQLException, RecordNotFindException, ClassNotFoundException {
		SQLExcecutor       excecutor   = SQLExcecutor.getInstance();
		String             query       = "SELECT * FROM main.\"students\"";
		ResultSet          res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Student> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Student(res));
		}
		return resultArray;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return this.getId() + ": " + this.name + " (" + this.birthday.toString() + ") " + (sex == 0 ? "Ж" : "М");
	}

	public void save() throws SQLException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "UPDATE main.students " +
		               "SET name = '" + this.getName() + "', birthday = '" + birthday.toString() + " ', sex = " +
		               this.getSex() + ", groupfk = " + this.getGroup().getId() +
		               " WHERE id = "+ this.getId() +" RETURNING \"id\";";
		excecutor.excecuteQuery("Innopolis", query, new String[] {});
	}
}
