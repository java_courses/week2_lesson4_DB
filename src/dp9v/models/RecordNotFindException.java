package dp9v.models;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class RecordNotFindException extends Exception {
	public RecordNotFindException(String message) {
		super(message);
	}
}
