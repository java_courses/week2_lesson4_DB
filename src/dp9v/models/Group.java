package dp9v.models;

import java.security.PublicKey;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class Group extends IDRecord {
	private String name;
	private long   number;

	private Group(ResultSet set) throws SQLException {
		super(set.getLong("ID"));
		this.name = set.getString("Name");
		this.number = set.getLong("Number");
	}

	public Group(String name, long number) throws SQLException, RecordNotCreateException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "INSERT INTO main.\"Group\" (\"Name\", \"Number\") VALUES ('" + name + "', " + number +
		               ") RETURNING \"ID\"";
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			this.name = name;
			this.number = number;
			this.setId(res.getLong("ID"));
		} else
			throw new RecordNotCreateException("не удалось создать запись");
	}

	public static Group get(long id) throws ClassNotFoundException, SQLException, RecordNotFindException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"Group\"\n" +
		               "WHERE \"Group\". \"ID\" = " + id;
		ResultSet res = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		if(res.next()) {
			Group group = new Group(res);
			return group;
		} else
			throw new RecordNotFindException("Не найдена запись");
	}

	public static ArrayList<Group> filterByName(String name) throws SQLException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query = "SELECT *\n" +
		               "FROM main.\"Group\"\n" +
		               "WHERE \"Group\".\"Name\" ='" + name + "'";
		ResultSet        res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Group> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Group(res));
		}
		return resultArray;
	}

	public static ArrayList<Group> all() throws SQLException {
		SQLExcecutor     excecutor   = SQLExcecutor.getInstance();
		String           query       = "SELECT * FROM main.\"Group\";";
		ResultSet        res         = excecutor.excecuteQuery("Innopolis", query, new String[] {});
		ArrayList<Group> resultArray = new ArrayList<>();
		while(res.next()) {
			resultArray.add(new Group(res));
		}
		return resultArray;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public void save() throws SQLException {
		SQLExcecutor excecutor = SQLExcecutor.getInstance();
		String query =
				"UPDATE main.\"Group\" SET \"Name\" = '" + this.getName() + "', \"Number\" = " + this.getNumber() +
				" WHERE \"ID\" = " + this.getId() + ";";
		excecutor.excecuteQuery("Innopolis", query, new String[] {});
	}
}
