package dp9v.models;

import java.sql.*;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class SQLExcecutor {
	private static SQLExcecutor ourInstance = new SQLExcecutor();
	private final  String       HOST_NAME   = "127.0.0.1";
	private final  String       PORT        = "5432";
	private final  String       LOGIN       = "postgres";
	private final  String       PASSWORD    = "11235815";

	private SQLExcecutor(){
	}

	public static SQLExcecutor getInstance() {
		return ourInstance;
	}

	private Connection open(String dbName) throws SQLException {
		String url = "jdbc:postgresql://" + HOST_NAME + ":" + PORT + "/" + dbName;
		Connection con = DriverManager.getConnection(url, LOGIN, PASSWORD);
		return con;
	}


	public ResultSet excecuteQuery(String dbName, String query, String[] args) throws SQLException{
		Statement sqlQuery;
		try(Connection con = open(dbName)){
			if(args.length>0){
				sqlQuery = con.prepareStatement(query);
				for(int i=0; i<args.length;i++){
					((PreparedStatement) sqlQuery).setString(i+1,args[i]);
				}
				return ((PreparedStatement) sqlQuery).executeQuery();
			}
			else {
				sqlQuery = con.createStatement();
				return sqlQuery.executeQuery(query);
			}
		}

	}
}
