package dp9v.models;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class IDRecord {
	private long id;

	public IDRecord(long id) {
		this.id = id;
	}

	protected IDRecord(){}
	protected void setId(long id){
		this.id = id;
	}

	public long getId() {
		return id;
	}

}
