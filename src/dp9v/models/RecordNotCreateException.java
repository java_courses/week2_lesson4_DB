package dp9v.models;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class RecordNotCreateException extends Exception {
	public RecordNotCreateException(String message) {
		super(message);
	}
}
