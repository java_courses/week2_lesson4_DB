package dp9v;

import dp9v.models.*;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by dpolkovnikov on 16.02.17.
 */
public class Innopolis {
	public static void main(
			String[] args) throws ClassNotFoundException, SQLException, RecordNotCreateException, RecordNotFindException {

	}

	private static void createStudents() throws RecordNotFindException, SQLException, ClassNotFoundException, RecordNotCreateException {
		Group              group13    = Group.get(13);
		Group              group14    = Group.get(14);
		Group              group15    = Group.get(15);
		Student            student1_1 = new Student("Student1", 1, Date.valueOf("1993-03-22"), group13);
		Student            student1_2 = new Student("Student2", 0, Date.valueOf("1994-04-22"), group13);
		Student            student1_3 = new Student("Student3", 1, Date.valueOf("1992-02-22"), group13);
		Student            student2_1 = new Student("Student1", 0, Date.valueOf("1993-03-03"), group14);
		Student            student2_2 = new Student("Student2", 1, Date.valueOf("1994-04-07"), group14);
		Student            student2_3 = new Student("Student3", 0, Date.valueOf("1992-02-16"), group14);
		Student            student3_1 = new Student("Student1", 0, Date.valueOf("1993-12-23"), group15);
		Student            student3_2 = new Student("Student2", 0, Date.valueOf("1994-11-02"), group15);
		Student            student3_3 = new Student("Student3", 1, Date.valueOf("1992-06-28"), group15);
		ArrayList<Student> sts        = Student.filterByName("Student1");
		for(Student s : sts) {
			System.out.println(s);
		}
		System.out.println("----");
		sts = Student.all();
		for(Student s : sts) {
			System.out.println(s);
		}
		System.out.println("----");
		Student st = Student.get(student1_1.getId());
		System.out.println(st.toString());
		st.setName("Student228");
		st.setGroup(group15);
		st.setSex(0);
		st.setBirthday(Date.valueOf("2000-04-14"));
		st.save();
		st = Student.get(student1_1.getId());
		System.out.println(st.toString());
	}

	private static void createGroups() throws SQLException, RecordNotCreateException, RecordNotFindException, ClassNotFoundException {
		Group            group1 = new Group("group1", 1488);
		Group            group2 = new Group("group2", 2218);
		Group            group3 = new Group("group3", 911);
		Group            group4 = new Group("group1", 1823);
		ArrayList<Group> groups = Group.filterByName("group1");
		for(Group group :
				groups) {
			System.out.println(group.getId() + ":" + group.getName() + " - " + group.getNumber());
		}
		System.out.println("------------");
		groups = Group.all();
		for(Group group :
				groups) {
			System.out.println(group.getId() + ":" + group.getName() + " - " + group.getNumber());
		}
		Group group = Group.get(group1.getId());
		System.out.println(group.getId() + ":" + group.getName() + " - " + group.getNumber());
	}

	private static void createLection() throws RecordNotFindException, SQLException, RecordNotCreateException {
		Lection lec1 = new Lection("lection1", "text1");
		Lection lec2 = new Lection("lection2", "text2");
		Lection lec3 = new Lection("lection3", "text3");
		Lection lec4 = new Lection("lection4", "text4");
		Lection lec5 = new Lection("lection5", "text5");
		ArrayList<Lection> lects =  Lection.filterByName("lection4");
		for(Lection lect: lects)
			System.out.println(lect.getId());
		lects =  Lection.all();
		for(Lection lect: lects)
			System.out.println(lect.getId());
		Lection lee = Lection.get(lec1.getId());
		lee.setText("llooooll");
		lee.setName("lection5");
		lee.save();
	}
}
